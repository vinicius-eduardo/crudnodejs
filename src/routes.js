const express = require('express');
const routes = express.Router();

const ExplorersController = require('./constrollers/ExplorersController');

routes.post('/explorers', ExplorersController.register);
routes.get('/explorers', ExplorersController.show);
routes.put('/explorers/:id', ExplorersController.update);
routes.delete('/explorers/:id', ExplorersController.destroy);
routes.post('/explorers/:id/stack', ExplorersController.addStack);

module.exports = routes;