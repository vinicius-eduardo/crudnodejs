var users = require('../../app');

module.exports = {
	show(req, res){
		
		const result = users.map( function (item) { 
			const { name, stack } = item;
			return { name, stack };
		});
		
		res.send(result);
	},

	update(req, res){
		
		const id = users.find(function (item) {
			return item.id == req.params.id;
		});
		
		const index = users.indexOf(id);
		
		users[index].name = req.body.name;
		
		res.status(200).send('Value updated');
	},
	
	register(req, res){

		const { id, name} = req.body;
		
		const newUser = { id, name, stack: []};

		users.push(newUser);

		res.status(200).send('New user registered');
	},

	addStack(req, res){
		const id = users.find(function (item) {
			return item.id == req.params.id;
		});
		
		const index = users.indexOf(id);
		
		users[index].stack.push(req.body.name);
		
		res.status(200).send('Stack Added');
	},

	destroy(req, res){
		const id = users.find(function (item) {
			return item.id == req.params.id;
		});
		
		const index = users.indexOf(id);
		
		users.splice(index, index + 1);
		
		res.status(200).send('Removed.');
	}
};