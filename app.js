const express = require('express');

const app = express();
app.use(express.json());	//permite manipular req.body no PUT

var users = [];
module.exports = users;

app.use('/explorers/:id', (req, res, next) => {

	const id = users.find( function (item) {
		return item.id == req.params.id;
	});

	if( !id )
		res.status(404).send('User not found');
	
	next();
});

app.use('/', require('./src/routes'));


app.listen(3001, function(){
	console.log('Server Running...');
});